#include <main.h>
#include "cmsis_os.h"
#include <stdbool.h>
#include <string.h>
#include "stm32f4xx.h"
#include "canNet.h"
#if HEAD_BOARD == 1
#include "../headDevice/headDeviceDebugPort.h"
#endif
#if LED_BOARD == 1
#include "ledTask.h"
#endif
#if MOTORS_BOARD == 1
#include "motors_script.h"
#include "motors.h"
#endif
#include "flash.h"
#include "firmwarer.h"



CanNetState canNetState = CanNetState_Init;


CanNetMessage CanNet_CreateLedCommand_DrawRawPixels(uint8_t channel, uint8_t offset, uint32_t pixels);
CanNetMessage CanNet_CreateLedCommand(uint8_t channel, LedUnit_LedCommandOpcode action, bool reverse);
bool CanNetSendMessage(CanNetMessage message);


void CanNet_MotorMovingBegin(uint8_t motorNumber, bool reverse){
	CanNetSendMessage(CanNet_CreateLedCommand(motorNumber, LedUnit_LedCommandOpcode_DarkSpotRun, reverse));
}
void CanNet_MotorMovingStop(uint8_t motorNumber){
	CanNetSendMessage(CanNet_CreateLedCommand(motorNumber, LedUnit_LedCommandOpcode_SLowLightSpotRun, false));
}

CAN_RxHeaderTypeDef testRcvHeader;
uint8_t testRcvPayload[64];




RawCanRxStruct rawCanRxQueue[RawCanRxqueueLen];
uint8_t rawCanRxQueuePutIndex = 0;
uint8_t rawCanRxQueueGetIndex = 0;

void RawCanRxPut(RawCanRxStruct rx){
	rawCanRxQueue[rawCanRxQueuePutIndex] = rx;

	if(++rawCanRxQueuePutIndex >= RawCanRxqueueLen)
		rawCanRxQueuePutIndex = 0;
}

RawCanRxStruct* RawCanRxGet(void){
	if(rawCanRxQueuePutIndex == rawCanRxQueueGetIndex)
		return NULL;
	RawCanRxStruct* result = &rawCanRxQueue[rawCanRxQueueGetIndex];
	if(++rawCanRxQueueGetIndex >= RawCanRxqueueLen)
		rawCanRxQueueGetIndex = 0;
	return result;
}


void CanNetParser(RawCanRxStruct* rx){
	uint8_t boardNum = rx->rcvHeader.ExtId >> 24 & 0x0F;
	uint32_t opcode = rx->rcvHeader.ExtId & 0xFFFFFF;

#if LED_BOARD == 1



	if(rx->rcvHeader.ExtId >= CAN_NET_ADDR_LED_CH1_SET_COMMAND && rx->rcvHeader.ExtId < CAN_NET_ADDR_LED_CH1_SET_COMMAND + 0x100){
		//LEDs channel 1
		LedStripSetLed(0, rx->rcvHeader.ExtId & 0xFF, rx->rcvPayload[0], rx->rcvPayload[1], rx->rcvPayload[2]);

	}else if(rx->rcvHeader.ExtId >= CAN_NET_ADDR_LED_CH2_SET_COMMAND && rx->rcvHeader.ExtId < CAN_NET_ADDR_LED_CH2_SET_COMMAND + 0x100){
		//LEDs channel 2
		LedStripSetLed(1, rx->rcvHeader.ExtId & 0xFF, rx->rcvPayload[0], rx->rcvPayload[1], rx->rcvPayload[2]);
	}
	else if(rx->rcvHeader.ExtId >= CAN_NET_ADDR_LED_CH3_SET_COMMAND && rx->rcvHeader.ExtId < CAN_NET_ADDR_LED_CH3_SET_COMMAND + 0x100){
		//LEDs channel 3
		LedStripSetLed(2, rx->rcvHeader.ExtId & 0xFF, rx->rcvPayload[0], rx->rcvPayload[1], rx->rcvPayload[2]);
	}
	else if(rx->rcvHeader.ExtId >= CAN_NET_ADDR_LED_CH4_SET_COMMAND && rx->rcvHeader.ExtId < CAN_NET_ADDR_LED_CH4_SET_COMMAND + 0x100){
		//LEDs channel 4
		LedStripSetLed(3, rx->rcvHeader.ExtId & 0xFF, rx->rcvPayload[0], rx->rcvPayload[1], rx->rcvPayload[2]);
	}
	else if(rx->rcvHeader.ExtId >= CAN_NET_ADDR_LED_CH5_SET_COMMAND && rx->rcvHeader.ExtId < CAN_NET_ADDR_LED_CH5_SET_COMMAND + 0x100){
		//LEDs channel 5
		LedStripSetLed(4, rx->rcvHeader.ExtId & 0xFF, rx->rcvPayload[0], rx->rcvPayload[1], rx->rcvPayload[2]);
	}
	else if(opcode == CAN_NET_ADDR_SET_LEDS_Y_COMMAND){
		LedStripSetY(rx->rcvPayload[0]);
	}
	else if(opcode == CAN_NET_ADDR_SET_LED_HSV_COMMAND){
		hsv hsv;
		hsv.h = (((uint16_t)rx->rcvPayload[2]) << 8) | rx->rcvPayload[3];
		hsv.s = rx->rcvPayload[4];
		hsv.v = rx->rcvPayload[5];
		LedStripSetHSVLED(rx->rcvPayload[0], rx->rcvPayload[1], hsv);
	}
	else if(opcode == CAN_NET_ADDR_SET_ARRAY_LED_HSV_COMMAND){
		hsv hsv;
		hsv.h = (((uint16_t)rx->rcvPayload[2]) << 8) | rx->rcvPayload[3];
		hsv.s = rx->rcvPayload[4];
		hsv.v = rx->rcvPayload[5];
		uint8_t ledCnt  = rx->rcvPayload[6];
		LedStripSetHSVARRAYLED(rx->rcvPayload[0], rx->rcvPayload[1], ledCnt, hsv);
	}
	else if(opcode == CAN_NET_ADDR_START_Y_ANIMATION_COMMAND){

		uint8_t channel = rx->rcvPayload[0];
		int8_t offset = rx->rcvPayload[1];
		int8_t len = rx->rcvPayload[2];
		uint8_t patternIndex = rx->rcvPayload[3];
		uint8_t speed_ledPerSecond = rx->rcvPayload[4];
		uint8_t direction = rx->rcvPayload[5];

		LedStripBeginYAnimation(channel, patternIndex, offset, len, speed_ledPerSecond, direction);
	}


#endif



#if MOTORS_BOARD == 1

	if(boardNum != motorsBoardNum)
		return;


	if(opcode == CAN_NET_ADDR_SCRIPT_RUN_COMMAND){
		// run motor script
		MotorsScript_SetEnableCycleScripts(0);
		MotorsScript_StopScripts();
		MotorsScript_RunScript(rx->rcvPayload[0]);
	}

	if(opcode == CAN_NET_ADDR_MOTORS_RUN_COMMAND){
		// run motor script
		MotorsScript_SetEnableCycleScripts(0);
		MotorsScript_StopScripts();
		uint8_t motorNum = MIN(rx->rcvPayload[0], 2);
		uint8_t motorDir = MIN(rx->rcvPayload[1], 1);

		if(motorDir)
			MotorClose(motorNum, 1000, 100);
		else
			MotorOpen(motorNum, 1000, 100);
	}

	if(opcode == CAN_NET_ADDR_SCRIPT_CHECK_COMMAND){
		uint8_t scriptNum = MIN(rx->rcvPayload[0], 2);
		uint16_t len = ((uint16_t)rx->rcvPayload[1] << 8) | rx->rcvPayload[2];
		uint16_t crc = ((uint16_t)rx->rcvPayload[3] << 8) | rx->rcvPayload[4];
		int check = MotorsScript_CheckScript(scriptNum, len, crc);
		CanNetMessage answer;
		answer.payloadLen = 2;
		answer.extId = CAN_NET_ADDR_SCRIPT_CHECK_ANSWER +  + (boardNum << 24);;
		answer.payload[0] = check >> 8;
		answer.payload[1] = check >> 0;
		CanNetSendMessage(answer);

	}

	if(opcode >= CAN_NET_ADDR_SCRIPT_0_WRITE_COMMAND && opcode <= CAN_NET_ADDR_SCRIPT_0_WRITE_ANSWER){
		//dinamic script 0
		uint16_t offset = opcode - CAN_NET_ADDR_SCRIPT_0_WRITE_COMMAND;
		MotorsScript_EditDynamicScrip(0, (char*)rx->rcvPayload, (uint16_t)offset, rx->rcvHeader.DLC);

		CanNetMessage answer;
		answer.payloadLen = rx->rcvHeader.DLC;
		answer.extId = CAN_NET_ADDR_SCRIPT_0_WRITE_ANSWER + offset + (boardNum << 24);
		MotorsScript_ReadDynamicScrip(0, (char*)answer.payload, (uint16_t)offset, rx->rcvHeader.DLC);
		CanNetSendMessage(answer);

		MotorsScript_SetEnableCycleScripts(0);
	}
	if(opcode >= CAN_NET_ADDR_SCRIPT_1_WRITE_COMMAND && opcode <= CAN_NET_ADDR_SCRIPT_1_WRITE_ANSWER){
		//dinamic script 1
		uint16_t offset = opcode - CAN_NET_ADDR_SCRIPT_1_WRITE_COMMAND;
		MotorsScript_EditDynamicScrip(1, (char*)rx->rcvPayload, (uint16_t)offset, rx->rcvHeader.DLC);
		CanNetMessage answer;
		answer.payloadLen = rx->rcvHeader.DLC;
		answer.extId = CAN_NET_ADDR_SCRIPT_1_WRITE_ANSWER + offset + (boardNum << 24);
		MotorsScript_ReadDynamicScrip(1, (char*)answer.payload, (uint16_t)offset, rx->rcvHeader.DLC);
		CanNetSendMessage(answer);

		MotorsScript_SetEnableCycleScripts(0);
	}
	if(opcode == CAN_NET_CONFIG_CURR_LIMIT){
		//Motors current limit save
		Motors_SETMotorsCurrentLimits(rx->rcvPayload);

		MotorsScript_SetEnableCycleScripts(0);
	}


	if(opcode == CAN_NET_ADDR_FIRMWARE_BEGIN){
		DataStorage_ClearFile(DataStorageType_MCU_Firmware);
	}

	if(opcode >= CAN_NET_ADDR_FIRMWARE_START && opcode < CAN_NET_ADDR_FIRMWARE_END){
		uint32_t offset = opcode - CAN_NET_ADDR_FIRMWARE_START;
		DataStorage_WriteFile_WithoutErase(DataStorageType_MCU_Firmware, (uint32_t)offset, rx->rcvHeader.DLC, rx->rcvPayload);

		CanNetMessage answer;
		answer.payloadLen = 0;
		answer.extId = CAN_NET_ADDR_FIRMWARE_ACK + (boardNum << 24);
		CanNetSendMessage(answer);

	}
	if(opcode == CAN_NET_ADDR_ERRORS_READ_COMMAND){
		uint16_t readedErrorCnt = 0;
		uint16_t offset = 0;

		do{
			uint16_t _errors[2];
			readedErrorCnt = Motors_ReadErrors(_errors, offset, arraysize(_errors));
			if(readedErrorCnt){
				CanNetMessage answer;
				answer.payloadLen = 0;
				answer.extId = CAN_NET_ADDR_ERRORS_READ_ANSWER + offset*2 + (boardNum << 24);
				for(int e = 0; e < readedErrorCnt; e++){
					answer.payload[offset*2 + 0] = _errors[e] >> 8;
					answer.payload[offset*2 + 1] = _errors[e] >> 0;
					answer.payloadLen +=2;
				}
				CanNetSendMessage(answer);
			}

		}while(readedErrorCnt != 0);
	}
	if(opcode == CAN_NET_ADDR_ERRORS_RESET_COMMAND){
		Motors_ResetErrors();
	}

	if(opcode == CAN_NET_ADDR_SCRIPT_READ_COMMAND){

		//read script 0
		uint32_t maxLen = 0x500;
		uint32_t offset = 0;

		while(offset < maxLen){
			CanNetMessage answer;

			answer.extId = CAN_NET_ADDR_SCRIPT_0_WRITE_ANSWER + offset + (boardNum << 24);
			MotorsScript_ReadDynamicScrip(0, (char*)answer.payload, (uint16_t)offset, 8);
			answer.payloadLen = 8;
			for(int i = 0; i < 8; i++){
				if(answer.payload[i] == 0 || answer.payload[i] == 0xFF){
					answer.payloadLen = i;
					offset = maxLen;
					break;
				}
			}
			CanNetSendMessage(answer);
			offset += 8;
			vTaskDelay(pdMS_TO_TICKS(5));
		}

		offset = 0;
		while(offset < maxLen){
			CanNetMessage answer;

			answer.extId = CAN_NET_ADDR_SCRIPT_1_WRITE_ANSWER + offset + (boardNum << 24);
			MotorsScript_ReadDynamicScrip(1, (char*)answer.payload, (uint16_t)offset, 8);
			answer.payloadLen = 8;
			for(int i = 0; i < 8; i++){
				if(answer.payload[i] == 0 || answer.payload[i] == 0xFF){
					answer.payloadLen = i;
					offset = maxLen;
					break;
				}
			}
			CanNetSendMessage(answer);
			offset += 8;
			vTaskDelay(pdMS_TO_TICKS(5));
		}

		{

			CanNetMessage answer;

			answer.extId = CAN_NET_ADDR_CONFIG_CURR_LIMIT_ANSWER + (boardNum << 24);
			answer.payloadLen = MOTORS_CHANNELS_CNT;
			Motors_GETMotorsCurrentLimits(answer.payload);
			CanNetSendMessage(answer);
		}


	}





	if(opcode == CAN_NET_ADDR_FIRMWARE_END){
		uint8_t fwSignature[38];
		DataStorage_ReadFile(DataStorageType_MCU_Firmware, 0x2000, 38, fwSignature);

		if (memcmp(fwSignature, "MOTORSB_SIGNATURE_With_Len_Of_38_Bytes", 38) == 0)
		{
			Firmwarer(DataStorage_CalculateAddree(DataStorageType_MCU_Firmware, 0), 1000);
			NVIC_SystemReset();
		}
	}




#endif
#if HEAD_BOARD == 1
	HeadDeviceOnCanRcved(rx);
#endif
}


void CanNet(void){
	canNetState = CanNetState_Init;


	canNetState = CanNetState_Idle;
	while(true){
		while(1){
			RawCanRxStruct* rcvCan = RawCanRxGet();
			if(rcvCan == NULL)
				break;
			CanNetParser(rcvCan);
		}
/*
		static uint32_t testTick = 0;
		static uint32_t startDelayTick = 2000;
		uint32_t opcode = 0x2500;
		static offset = 0;

		if((HAL_GetTick() - testTick) > 10 && (HAL_GetTick() > 2000)){
			testTick = HAL_GetTick();

			RawCanRxStruct rcvCan;
			rcvCan.rcvHeader.ExtId = opcode + offset;
			rcvCan.rcvHeader.DLC = 8;

			for(int b = 0; b < rcvCan.rcvHeader.DLC; b++)
				rcvCan.rcvPayload[b] = 0x30 + b;
			CanNetParser(&rcvCan);

			offset += 8;

		}
*/

		vTaskDelay(pdMS_TO_TICKS(5));
	}
}


extern CAN_HandleTypeDef hcan2;
bool transmitting = false;

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef* hcan) {
	RawCanRxStruct rx;
	HAL_CAN_GetRxMessage(&hcan2, CAN_RX_FIFO0, &rx.rcvHeader, rx.rcvPayload);
	//HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &testRcvHeader, testRcvPayload);
	RawCanRxPut(rx);
}


void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef* hcan) {
	HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO1, &testRcvHeader, testRcvPayload);
}



void HAL_CAN_TxMailbox0CompleteCallback(CAN_HandleTypeDef* hcan) {
	transmitting = false;
}

void HAL_CAN_TxMailbox1CompleteCallback(CAN_HandleTypeDef* hcan) {

}

void HAL_CAN_TxMailbox2CompleteCallback(CAN_HandleTypeDef* hcan) {

}

bool CanNetSendMessage(CanNetMessage message){

	CAN_TxHeaderTypeDef header;

	header.IDE = CAN_ID_EXT;
	header.StdId = message.extId;
	header.ExtId = message.extId;
	header.DLC = message.payloadLen;
	header.RTR = CAN_RTR_DATA;

	uint32_t tick = HAL_GetTick();

	while(HAL_CAN_GetTxMailboxesFreeLevel(&hcan2) == 0 && (HAL_GetTick() - tick) < 10 );

	if(HAL_CAN_GetTxMailboxesFreeLevel(&hcan2) == 0)
		return false;

	transmitting = true;
	if(HAL_CAN_AddTxMessage(&hcan2, &header, message.payload, CAN_TX_MAILBOX0) != HAL_OK){
		transmitting = false;
		return false;
	}


	while(transmitting && (HAL_GetTick() - tick) < 10 );
	return !transmitting;
}





CAN_BR_parameters CAN_Analizer_CalcullateCANBrparameters(StandartCanBR br) {
	CAN_BR_parameters result;
/*
	LL_RCC_ClocksTypeDef rcc_clocks;
	LL_RCC_GetSystemClocksFreq(&rcc_clocks);

	if (rcc_clocks.PCLK1_Frequency != 30000000) {
		result.BS1 = CAN_BS1_5TQ;
		result.BS2 = CAN_BS2_4TQ;
		result.SJW = CAN_SJW_3TQ;
		result.Prescaller = (rcc_clocks.PCLK1_Frequency) / (br * 10);
		return result;
	}
*/
	if (br == 1000000) {
		result.Prescaller = 16;
		result.BS1 = CAN_BS1_15TQ;
		result.BS2 = CAN_BS2_2TQ;
		result.SJW = CAN_SJW_1TQ;
		return result;
	}

	if (br == 500000) {
		result.Prescaller = 32;
		result.BS1 = CAN_BS1_15TQ;
		result.BS2 = CAN_BS2_2TQ;
		result.SJW = CAN_SJW_1TQ;
		return result;
	}
	if (br == 250000) {
		result.Prescaller = 64;
		result.BS1 = CAN_BS1_15TQ;
		result.BS2 = CAN_BS2_2TQ;
		result.SJW = CAN_SJW_1TQ;
		return result;
	}
	if (br == 125000) {
		result.Prescaller = 128;
		result.BS1 = CAN_BS1_15TQ;
		result.BS2 = CAN_BS2_2TQ;
		result.SJW = CAN_SJW_1TQ;
		return result;
	}
	if (br == 50000) {
		result.Prescaller = 320;
		result.BS1 = CAN_BS1_15TQ;
		result.BS2 = CAN_BS2_2TQ;
		result.SJW = CAN_SJW_1TQ;
		return result;
	}
	else if (br == 33333) {
		result.Prescaller = 480;
		result.BS1 = CAN_BS1_15TQ;
		result.BS2 = CAN_BS2_2TQ;
		result.SJW = CAN_SJW_1TQ;
		return result;
	}
	else if (br == 83333) {
		result.Prescaller = 192;
		result.BS1 = CAN_BS1_15TQ;
		result.BS2 = CAN_BS2_2TQ;
		result.SJW = CAN_SJW_1TQ;
		return result;
	}
/*
	for (int BS1 = 5; BS1 < 12; BS1++) {
		for (int BS2 = 4; BS2 < 9; BS2++) {
			uint32_t relation = ((uint32_t) BS1 * 100) / ((uint32_t) (BS1 + BS2));
			if (relation > 50 && relation < 90) {
				int quantumsInOneBit = 1 + BS1 + BS2;
				for (int prescaller = 1; prescaller < 1025; prescaller++) {
					if (rcc_clocks.PCLK1_Frequency / (quantumsInOneBit * prescaller) == br) {
						result.Prescaller = prescaller;
						result.BS1 = Helper_ValToBS1Mask(BS1);
						result.BS2 = Helper_ValToBS2Mask(BS2);
						return result;
					}
				}
			}
		}
	}
*/
	result.BS1 = CAN_BS1_15TQ;
	result.BS2 = CAN_BS2_2TQ;
	result.SJW = CAN_SJW_1TQ;
	return result;
}



