/*
 * canNet.h
 *
 *  Created on: Feb 25, 2023
 *      Author: flogg
 */

#ifndef CANNET_H_
#define CANNET_H_

#include <stdbool.h>

typedef enum{
	CanNetState_Init,
	CanNetState_Idle,

}CanNetState;

typedef struct{
	uint32_t extId;
	uint8_t payloadLen;
	uint8_t payload[64];
}CanNetMessage;


typedef enum{
	LedUnit_LedCommandOpcode_DrowRawPixels,
	LedUnit_LedCommandOpcode_SLowLightSpotRun,
	LedUnit_LedCommandOpcode_LightSpotRun,
	LedUnit_LedCommandOpcode_DarkSpotRun,
}LedUnit_LedCommandOpcode;


typedef struct {
	uint8_t ledCnt;
	uint8_t argb[3];
	bool reverse;
}LedUnit_LedCommandOpcode_SpotRunArgs;


typedef struct{
	uint8_t channel;
	uint8_t offset;
	uint8_t pixelCnt;
	uint8_t pixels[30];
}LedUnit_LedCommandOpcode_DrawRawPixels;


typedef  struct{
	uint8_t channel;
	LedUnit_LedCommandOpcode opcode;
	union{
		LedUnit_LedCommandOpcode_DrawRawPixels drawRawPixelsArgs;
		LedUnit_LedCommandOpcode_SpotRunArgs ligntSporArgs;
		LedUnit_LedCommandOpcode_SpotRunArgs darkSpotArgs;
	};
}LedUnit_LedCommand;


typedef struct __packed
{
	char FirmwareSignature[38];
	char FirmwareVersion_date[11];
	char FirmwareVersion_space;
	char FirmwareVersion_time[12];
}signatureAndVersionType;

typedef struct{
	uint32_t Prescaller;
	uint32_t BS1;
	uint32_t BS2;
	uint32_t SJW;
}CAN_BR_parameters;

typedef enum{
	StandartCanBR_1000000 = 1000000,
	StandartCanBR_500000 = 500000,
	StandartCanBR_250000 = 250000,
	StandartCanBR_125000 = 125000,
	StandartCanBR_50000 = 50000,
	StandartCanBR_33333 = 33333,
	StandartCanBR_83333 = 83333,

}StandartCanBR;
CAN_BR_parameters CAN_Analizer_CalcullateCANBrparameters(StandartCanBR br);

#define RawCanRxqueueLen 10
typedef struct{
	CAN_RxHeaderTypeDef rcvHeader;
	uint8_t rcvPayload[8];
}RawCanRxStruct;

void RawCanRxPut(RawCanRxStruct rx);
bool CanNetSendMessage(CanNetMessage message);

CanNetMessage CanNet_CreateLedCommand_DrawRawPixels(uint8_t channel, uint8_t offset, uint32_t pixels);
CanNetMessage CanNet_CreateMotorsCommand_RunScript(uint8_t script);


#define CAN_NET_ADDR_LED_CH1_SET_COMMAND		0x0000		// 0x100 range
#define CAN_NET_ADDR_LED_CH2_SET_COMMAND		0x0100		// 0x100 range
#define CAN_NET_ADDR_LED_CH3_SET_COMMAND		0x0200		// 0x100 range
#define CAN_NET_ADDR_LED_CH4_SET_COMMAND		0x0300		// 0x100 range
#define CAN_NET_ADDR_LED_CH5_SET_COMMAND		0x0400		// 0x100 range



#define CAN_NET_ADDR_SCRIPT_RUN_COMMAND			0x1000
#define CAN_NET_ADDR_MOTORS_RUN_COMMAND			0x1001
#define CAN_NET_ADDR_SCRIPT_CHECK_COMMAND		0x1002
#define CAN_NET_ADDR_SCRIPT_CHECK_ANSWER		0x1003
#define CAN_NET_CONFIG_CURR_LIMIT				0x1004

#define CAN_NET_ADDR_SCRIPT_READ_COMMAND		0x1005

#define CAN_NET_ADDR_ERRORS_READ_COMMAND		0x1006
#define CAN_NET_ADDR_CONFIG_CURR_LIMIT_ANSWER	0x1007
#define CAN_NET_ADDR_ERRORS_READ_ANSWER			0x1010		// 0x10 range

#define CAN_NET_ADDR_ERRORS_RESET_COMMAND		0x1020


#define CAN_NET_ADDR_SCRIPT_0_WRITE_COMMAND		0x2000
#define CAN_NET_ADDR_SCRIPT_0_WRITE_ANSWER		0x2500

#define CAN_NET_ADDR_SCRIPT_1_WRITE_COMMAND		0x3000
#define CAN_NET_ADDR_SCRIPT_1_WRITE_ANSWER		0x3500

#define CAN_NET_ADDR_SET_LEDS_Y_COMMAND			0x4000
#define CAN_NET_ADDR_SET_LED_HSV_COMMAND		0x4001
#define CAN_NET_ADDR_SET_ARRAY_LED_HSV_COMMAND	0x4002
#define CAN_NET_ADDR_START_Y_ANIMATION_COMMAND	0x4003



#define CAN_NET_ADDR_CHN_1_STATE_ANSWER				0x10000
#define CAN_NET_ADDR_CHN_2_STATE_ANSWER				0x10001
#define CAN_NET_ADDR_CHN_3_STATE_ANSWER				0x10002
#define CAN_NET_ADDR_CHN_4_STATE_ANSWER				0x10003
#define CAN_NET_ADDR_POWER_SUPPLY_STATE_ANSWER		0x10004

#define CAN_NET_ADDR_FIRMWARE_BEGIN					0x7FFFE
#define CAN_NET_ADDR_FIRMWARE_ACK					0x7FFFF
#define CAN_NET_ADDR_FIRMWARE_START					0x80000
#define CAN_NET_ADDR_FIRMWARE_END					0xFFFFF



#define CAN_NET_MOTOR_SATE_Stop							0
#define CAN_NET_MOTOR_SATE_MalfunctionStop_HiLoad		1
#define CAN_NET_MOTOR_SATE_MalfunctionStop_NoLoad		2
#define CAN_NET_MOTOR_SATE_Opening						3
#define CAN_NET_MOTOR_SATE_Closing						4



#endif /* CANNET_H_ */
